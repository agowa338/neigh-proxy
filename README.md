This is a c++ version of the following shell script.
I was not able to use this shellscript because I needed to set the suid bit.

I added the following line to openvpn
learn-address /etc/openvpn/learn-address

But because of the dropped privileges (nobody:nobody) this script was executed as nobody.
Which is quite unhelpfull, if you want to run 'ip neigh replace proxy "$addr" dev eth0' from there.

I've added the shellscript to this repossitory, too.

In order to compile this program you need to run:
$ g++ learn-address-neigh
The output is saved as "a.out".

